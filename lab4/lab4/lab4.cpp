#include "stdafx.h"


#include <iostream>
#include <stdlib.h>

#include "integral.cpp"

using namespace std;

double function(double x) {
	return x * x;
}

int main() {
	setlocale(LC_ALL, "Russian");
	double a;
	double b;
	double n;

	cout << "Введите a: ";
	cin >> a;
	cout << "Введите b: ";
	cin >> b;
	cout << "Введите n: ";
	cin >> n;

	integral obj(function, a, b, n);

	int action = 0;

	while (1) {
		cout << "\n";
		cout << "1) Прямоугольники;" << endl;
		cout << "2) Трапеция;" << endl;
		cout << "3) Метод симпсон;" << endl;
		cout << "4) Метод монте-Карло;" << endl;
		cout << "5) Изменить параметры." << endl;
		cout << "6) Выход." << endl;
		cout << "Вводите действие: ";
		cin >> action;
		cout << "\n";
		switch (action) {
		case 1:
			cout << obj.rectangles() << endl;
			break;
		case 2:
			cout << obj.trapezoid() << endl;
			break;
		case 3:
			cout << obj.simpson() << endl;
			break;
		case 4:
			cout << obj.monteCarlo() << endl;
			break;
		case 5:
			cout << "Введите a: ";
			cin >> obj.a;
			cout << "Введите b: ";
			cin >> obj.b;
			cout << "Введите n: ";
			cin >> obj.n;

			obj.h = (obj.b - obj.a) / obj.n;

			obj.rectangles();

			break;
		case 6:
			return 0;
		}
	}

	system("pause");
	return 0;
}