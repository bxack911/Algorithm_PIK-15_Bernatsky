#ifndef INTEGRAL_H
#define INTEGRAL_H

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <iostream>

class integral
{
	public:
		double a;
		double b;
		double h;
		double n;
		double value;
	double (*func)(double);
	integral(double (*_func)(double), double _a, double _b, double _n);
	double rectangles();
	double trapezoid();
	double simpson();
	double monteCarlo();
};

#endif
