#include "integral.h"

integral::integral(double (*_func)(double), double _a=0, double _b=10, double _n=1){
	func=_func;
	a=_a;
	b=_b;
	n=_n;
	
	h=(b-a)/n;
	rectangles();
}

double integral::rectangles(){
	//����� ������������
	value = 0;
	double x;
	for (int i = 1; i <= n - 1; i++) {
        x = a + i * h; 
        value += func(x) * h; 
    }
    return value;
}

double integral::trapezoid(){
	//����� ��������
	value = 0;
	double x;
    for (int i = 1; i <= n - 1; i++) {
        x = a + i * h; 
        value += 2.0 * func(x); 
    }
    value = (value + func(a) + func(b)) *(h/2);
    return value;
}

double integral::simpson(){
	//����� ѳ������
	value = 0;
	double x; 
	x = a + h;
	while (x < b) {
		value += 4 * func(x);
		x += h;
		value += 2 * func(x);
		x += h;
	}
	value = h / 3 * (value + func(a) - func(b));
	return value;	
}

double integral::monteCarlo(){
	//����� �����-�����
	value = 0;
	
	double x; 
	double y; 
	
	double r1; 
	double r2; 
	
	int count = 0;
	int checkCount = 0;
	
	//�������� ������� �� ����
	srand(time(NULL));
	
	for (count=0; count < 10000*n; count++){
		//�������� �������� �����
	    r1 = (double)rand()/(double)RAND_MAX;
	    r2 = (double)rand()/(double)RAND_MAX;
	
	    x = ((b - a)*r1) + a;
	    y = (func(b)*r2) + 0;
	    
		//���������� �� ������� ����� � ������	
	    if (func(x) > y){
	      	checkCount++;
	    }
	}

  	value = (b - a)*func(b)*((double)checkCount/count);

    return value;
}


