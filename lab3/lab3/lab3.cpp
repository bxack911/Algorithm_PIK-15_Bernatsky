#include "stdafx.h"

#include <iostream>
#include <math.h>
#include <stdlib.h>

#include "randomize.cpp"

using namespace std;

int main() {
	setlocale(LC_ALL, "Russian");

	long long num;
	randomize rand;
	int *arr;
	int action = 0;

	cout << "Введите число: ";
	cin >> num;

	arr = rand.generateArrRand(num);

	while(1)
	{
		cout << "\n";
		cout << "1) Вывести массив" << endl;
		cout << "2) Частота интервалов появления;" << endl;
		cout << "3) Статистическая вероятность;" << endl;
		cout << "4) Дисперсия;" << endl;
		cout << "5) Среднеквадратическое отклонение;" << endl;
		cout << "6) Выход." << endl;
		cout << "Choose action: ";
		cin >> action;
		cout << "\n";
		switch (action) {
		case 1:
			for (int i = 0; i<rand.getSize(); i++) {
				if (i % 20 == 0) cout << "\n";
				cout << " " << arr[i];
			}
			cout << "\n";
			break;
		case 2:
			rand.statistic();
			break;
		case 3:
			cout << "Статистическая вероятность: " << rand.expectation() << endl;
			break;
		case 4:
			cout << "Дисперсия: " << rand.dispersion() << endl;
			break;
		case 5:
			cout << "Среднеквадратическое отклонение: " << rand.meanSquare() << endl;
			break;
		case 0:
			return 0;
		}
	}

	system("pause");
	return 0;
}


