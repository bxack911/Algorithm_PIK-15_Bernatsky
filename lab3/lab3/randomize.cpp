#include "randomize.h"
#include <iostream>

using namespace std;

randomize::randomize(){
    this->a = 1664525;
    this->c = 1013904223;
    this->m = (1<<31)-1;
    this->range = 150;
    this->size = 2000;

    this->arr = new int[size];
}

randomize::randomize(int _a, int _c, long long _m, int _range, int _size){
    this->a = _a;
    this->c = _c;
    this->m = _m;
    this->range = _range;
    this->size = _size;

    this->arr = new int[size];
}

int* randomize::generateArrRand(long long zero){
    long long elem=zero;
    arr[0] = elem*range/m;
    for (int i = 0; i < size; i++){
		elem = (a*elem + c) % m;
		arr[i] = elem*range/m;
	}
	return arr;
}

int randomize::getSize(){
    return size;
}

float* randomize::statistic(){
	float* stat = new float[range]; 
	int count = 0; 
	for (int i = 0; i <= range; i++) {
		count = 0; 
		for (int j = 0 ; j < size; j++){
			if (arr[j] == i) count++; 	
		}
		stat[i] = count/(float)size;
		cout<<"Numeral "<<i<<" have "<<count<<" count"<<" (Statistic "<<stat[i]<<")"<<endl; 
	}
	return stat;
}

float randomize::expectation(){
	cout.setstate(std::ios_base::failbit);
	float *stat=statistic();
	std::cout.clear();
	float exp = 0; 
	for (int i = 0; i <= range; i++) {
		exp += i*stat[i];
	}
	//free(stat);
	return exp;
}

float randomize::dispersion(){
	cout.setstate(std::ios_base::failbit);
	float *stat=statistic();
	std::cout.clear();
	float exp = expectation(); 
	float dis = 0; 
	for (int i = 0; i <= range; i++) {
		dis += (i - exp)*(i - exp)*stat[i];
	}
	//free(stat);
	return dis;
}

float randomize::meanSquare(){
	return sqrt(dispersion());
}

randomize::~randomize(){
    free(arr);
}

