#ifndef RANDOMIZE_H
#define RANDOMIZE_H

#include <stdlib.h>
#include <math.h>

class randomize{
    protected:
		long long a;
		long long c;
        long long m;
        int range;
        int size;

        int *arr;

    public:
        randomize();
        randomize(int _a, int _c, long long _m, int _range, int _size);
        int* generateArrRand(long long zero);
        int getSize();
        float* statistic();
        float expectation();
        float dispersion();
        float meanSquare();
        ~randomize();
};

#endif // RANDOMIZE_H

