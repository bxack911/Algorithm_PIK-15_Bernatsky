#include "stdafx.h"


#include <iostream>

using namespace std;

struct Date {
	unsigned int year : 18;
	unsigned int day : 5;
	unsigned int month : 4;
};

struct Time {
	unsigned int h : 21;
	unsigned int m : 4;
	unsigned int s : 25;
};

union SignedShort {
	short num;
	struct {
		unsigned short a : 15;
		unsigned short s : 1;
	} bite;
};

union dFloat {
	float num;
	struct {
		unsigned int a : 23;
		unsigned int b : 8;
		unsigned int c : 1;
	} part;
	struct {
		unsigned int a : 8;
		unsigned int b : 8;
		unsigned int c : 8;
		unsigned int d : 8;
	} byte;
	struct {
		unsigned int a1 : 1;
		unsigned int a2 : 1;
		unsigned int a3 : 1;
		unsigned int a4 : 1;
		unsigned int a5 : 1;
		unsigned int a6 : 1;
		unsigned int a7 : 1;
		unsigned int a8 : 1;
		unsigned int a9 : 1;
		unsigned int a10 : 1;
		unsigned int a11 : 1;
		unsigned int a12 : 1;
		unsigned int a13 : 1;
		unsigned int a14 : 1;
		unsigned int a15 : 1;
		unsigned int a16 : 1;
		unsigned int a17 : 1;
		unsigned int a18 : 1;
		unsigned int a19 : 1;
		unsigned int a20 : 1;
		unsigned int a21 : 1;
		unsigned int a22 : 1;
		unsigned int a23 : 1;
		unsigned int a24 : 1;
		unsigned int a25 : 1;
		unsigned int a26 : 1;
		unsigned int a27 : 1;
		unsigned int a28 : 1;
		unsigned int a29 : 1;
		unsigned int a30 : 1;
		unsigned int a31 : 1;
		unsigned int a32 : 1;
	} bite;
};

int main() {
	setlocale(LC_ALL, "Russian");

	int key;

	do{
		cout << "\n1) Задание 1; \n";
		cout << "2) Задание 2; \n";
		cout << "3) Задание 3; \n";
		cout << "4) Задание 4; \n";
		cout << "0) Выход:\n";
		cin >> key;

		switch (key) {
		case 1:
			Date date;
			Time time;


			date.day = 21;
			date.month = 2;
			date.year = 2018;

			time.h = 16;
			time.m = 18;
			time.s = 56;

			printf("\nДата: %d.%d.%d \nВремя: %d:%d:%d", date.day, date.month, date.year, time.h, time.m, time.s);
			printf("\n\nДата занимает %d байтов. Время занимает %d байтов\n", sizeof(date), sizeof(time));

			break;
		case 2:
		{
			signed short num = -5;

			SignedShort obj;
			obj.num = num;

			printf("Соединение: знак %c, значение %d\n", obj.bite.s ? '-' : '+', obj.bite.a);

			printf("Маска: знак %c, значение %d\n", (num & 0x8000) >> 7 ? '-' : '+', num & 0x7FFF);

			break;
		}
		case 3:

			printf("a) %d\n", (signed char)(5 + 127));
			printf("б) %d\n", (signed char)(2 - 3));
			printf("в) %d\n", (signed char)(-120 - 34));
			printf("г) %d\n", (signed char)(unsigned char)(-5));
			printf("д) %d\n", (signed char)(56 & 38));
			printf("е) %d\n", (signed char)(56 | 38));

			break;
		case 4:
		{

			float num = -12.517;

			dFloat objDFloat;
			objDFloat.num = num;

			printf("Значение в битах: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n",
				objDFloat.bite.a32,
				objDFloat.bite.a31,
				objDFloat.bite.a30,
				objDFloat.bite.a29,
				objDFloat.bite.a28,
				objDFloat.bite.a27,
				objDFloat.bite.a26,
				objDFloat.bite.a25,
				objDFloat.bite.a23,
				objDFloat.bite.a22,
				objDFloat.bite.a21,
				objDFloat.bite.a20,
				objDFloat.bite.a19,
				objDFloat.bite.a18,
				objDFloat.bite.a17,
				objDFloat.bite.a16,
				objDFloat.bite.a15,
				objDFloat.bite.a14,
				objDFloat.bite.a13,
				objDFloat.bite.a12,
				objDFloat.bite.a11,
				objDFloat.bite.a10,
				objDFloat.bite.a9,
				objDFloat.bite.a8,
				objDFloat.bite.a7,
				objDFloat.bite.a6,
				objDFloat.bite.a5,
				objDFloat.bite.a4,
				objDFloat.bite.a3,
				objDFloat.bite.a2,
				objDFloat.bite.a1
			);

			printf("Значения в байтах: %d %d %d %d\n",
				objDFloat.byte.d,
				objDFloat.byte.c,
				objDFloat.byte.b,
				objDFloat.byte.a
			);

			printf("Знак: %c; Мантиса: %d; Порядок обозначения: %d\n",
				objDFloat.byte.c ? '-' : '+',
				objDFloat.byte.b,
				objDFloat.byte.a
			);

			break;

		}
		default:
			return 0;
		}
	}while (key != 0);
}


