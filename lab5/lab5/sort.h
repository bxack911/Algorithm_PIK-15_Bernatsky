#ifndef SORT_H
#define SORT_H

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <algorithm>

class sort
{
	protected:
		int *A;
		int n;
		int k;
		int a;
		int b;
	public:
		sort();
		double bubble_sort();
		double paste_sort();
		double shell_sort();
		int increment(int inc[]);
		double choice_sort();
		void change(int _n, int _k, int _a, int _b);
		void randMas();
		~sort();
};

#endif

