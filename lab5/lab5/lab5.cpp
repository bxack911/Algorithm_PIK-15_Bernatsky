// ConsoleApplication1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"


#include <iostream>
#include <stdlib.h>

#include "sort.cpp"

using std::cout;
using std::cin;
using std::endl;

int main() {
	setlocale(LC_ALL, "Russian");
	int a;
	int b;
	int n;
	int k;

	cout << "Введите n: ";
	cin >> n;
	cout << "Введите k: ";
	cin >> k;
	cout << "Введите a: ";
	cin >> a;
	cout << "Введите b: ";
	cin >> b;

	sort obj;
	obj.change(n, k, a, b);

	int action = 0;

	while (1) {
		cout << "\n";
		cout << "1) Пузырьками;" << endl;
		cout << "2) Вставками;" << endl;
		cout << "3) Сортировка Шелла;" << endl;
		cout << "4) Выборка;" << endl;
		cout << "5) Изменить параметры;" << endl;
		cout << "6) Выход." << endl;
		cout << "Выберите действие: ";
		cin >> action;
		cout << "\n";
		switch (action) {
		case 1:
			cout << "Время сортировки пузырьками: " << obj.bubble() << " мс" << endl;
			break;
		case 2:
			cout << "Время сортировки вставками: " << obj.paste() << " мс" << endl;
			break;
		case 3:
			cout << "Время сортировки Шелла: " << obj.shell() << " мс" << endl;
			break;
		case 4:
			cout << "Время сортировки выборкой: " << obj.choice() << " мс" << endl;
			break;
		case 5:
			cout << "Введите n: ";
			cin >> n;
			cout << "Введите k: ";
			cin >> k;
			cout << "Введите a: ";
			cin >> a;
			cout << "Введите b: ";
			cin >> b;

			obj.change(n, k, a, b);
			break;
		case 6:
			return 0;
		}
	}

	system("pause");
	return 0;
}