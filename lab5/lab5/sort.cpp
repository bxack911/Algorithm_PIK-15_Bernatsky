#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>

class sort
{
protected:
	int *A;
	int n;
	int k;
	int a;
	int b;
public:
	sort();
	double bubble_sort();
	double paste_sort();
	double shell_sort();
	int increment(int inc[]);
	double choice_sort();
	void change(int _n, int _k, int _a, int _b);
	void randMas();
	~sort();
};

sort::sort() {
	n = 500;
	k = 100;
	a = 0;
	b = 10;
	A = new int[n];
	randMas();
}

double sort::bubble() {
	int m, i, j;
	clock_t start, mid, stop;

	start = clock();

	for (m = 0; m<k; m++) {
		randMas();
	}

	mid = clock();

	for (m = 0; m<k; m++) {
		randMas();

		for (i = 0; i < n - 1; i++)
			for (j = 0; j < n - i - 1; j++)
				if (A[j] > A[j + 1])
					std::swap(A[j], A[j + 1]);
	}

	stop = clock();

	double time = (double)(stop - mid) - (mid - start);

	return  time / k;
}

double sort::paste_sort() {
	int i, key, j, m;
	clock_t start, mid, stop;

	start = clock();

	for (m = 0; m<k; m++) {
		randMas();
	}

	mid = clock();

	for (m = 0; m<k; m++) {
		randMas();

		for (i = 1; i < n; i++) {
			key = A[i];
			j = i - 1;

			while (j >= 0 && A[j] > key) {
				A[j + 1] = A[j];
				j = j - 1;
			}
			A[j + 1] = key;
		}
	}

	stop = clock();

	double time = (double)(stop - mid) - (mid - start);

	return  time / k;
}

double sort::shell_sort() {
	int m, inc, i, j, seq[100];
	int s;

	clock_t start, mid, stop;

	start = clock();

	for (m = 0; m<k; m++) {
		randMas();
	}

	mid = clock();

	for (m = 0; m<k; m++) {
		randMas();

		s = increment(seq);
		while (s >= 0) {
			inc = seq[s--];

			for (i = inc; i < n; i++) {
				int temp = A[i];
				for (j = i - inc; (j >= 0) && (A[j] > temp); j -= inc)
					A[j + inc] = A[j];
				A[j + inc] = temp;
			}
		}
	}

	stop = clock();

	double time = (double)(stop - mid) - (mid - start);

	return  time / k;
}

int sort::increment(int inc[]) {
	int p1, p2, p3, s;

	p1 = p2 = p3 = 1;
	s = -1;
	do {
		if (++s % 2) {
			inc[s] = 8 * p1 - 6 * p2 + 1;
		}
		else {
			inc[s] = 9 * p1 - 9 * p3 + 1;
			p2 *= 2;
			p3 *= 2;
		}
		p1 *= 2;
	} while (3 * inc[s] < n);

	return s > 0 ? --s : 0;
}

double sort::choice_sort() {
	int m, i, j, min_idx;
	clock_t start, mid, stop;

	start = clock();

	for (m = 0; m<k; m++) {
		randMas();
	}

	mid = clock();

	for (m = 0; m<k; m++) {
		randMas();

		for (i = 0; i < n - 1; i++) {
			min_idx = i;
			for (j = i + 1; j < n; j++)
				if (A[j] < A[min_idx])
					min_idx = j;

			std::swap(A[min_idx], A[i]);
		}
	}

	stop = clock();

	double time = (double)(stop - mid) - (mid - start);

	return  time / k;
}

void sort::change(int _n, int _k, int _a, int _b) {
	n = _n;
	k = _k;
	a = _a;
	b = _b;
	delete(A);
	A = new int[n];
	randMas();
}

void sort::randMas() {
	srand(time(NULL));
	for (int i = 0; i < n; i++) {
		A[i] = a + rand() % (b - a + 1);
	}
}

sort::~sort() {
	delete(A);
}

