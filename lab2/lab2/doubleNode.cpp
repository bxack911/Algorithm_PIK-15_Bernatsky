#include "doubleNode.h"

//
doubleNode::doubleNode(){
	this->size = 0;
	this->head = this->tail = NULL; 
}

//   
int doubleNode::isEmpty(){
	return ((this->head == NULL) || (this->tail == NULL)); 
}

//   
int doubleNode::push(elem* data){
	node* obj = new node; 
	
	if (!obj){
		return 0;
	}
	
	obj->value = *data; 
	obj->next = this->head; 
	obj->prev = NULL; 
	
	if (!this->isEmpty()){
		this->head->prev = obj; 
	}
	else{
		this->tail = obj; 
	}
	
	this->head = obj; 
	this->size++;
	
	return 1; 
}

//   
int doubleNode::pop(elem* data){
	node* obj = NULL; 
	
	if (this->isEmpty()){
		return 0; 
	}
	
	obj = this->head; 
	this->head = this->head->next; 
	
	if (!this->isEmpty()){
		this->head->prev = NULL; 
	}
	else{
		this->tail = NULL;
	}
	
	*data = obj->value; 
	this->size--; 
	free(obj); 
	
	return 1; 
}

//   
int doubleNode::unshift(elem* data){
    node* obj = new node;
    
    if (!obj) {
        return 0;
    }

    obj->value = *data;
    obj->next = NULL;
    obj->prev = this->tail;
    
    if (!this->isEmpty()){
        this->tail->next = obj;
    }
    else{
        this->head = obj;
    }
    
    this->tail = obj;
    this->size++;
    
    return 1;
}

//   
int doubleNode::shift(elem* data){
    node *obj = NULL;

    if (this->isEmpty()){
        return 0;
    }

    obj = this->tail;
    this->tail = this->tail->prev;
    
    if (!this->isEmpty()){
        this->tail->next = NULL;
    }
    else {
        this->head = NULL;
    }

    *data = obj->value;
    this->size--;
    free(obj);

    return 1;
}

//  
node* doubleNode::getNode(int index){
    node *obj = NULL;
    int i;

    if (index >= this->size) {
        return NULL;
    }

    if (index < this->size/2){
        i = 0;
        obj = this->head;
        while (obj && i < index) {
            obj = obj->next;
            i++;
        }
    }
    else{
        i = this->size - 1;
        obj = this->tail;
        while (obj && i > index) {
            obj = obj->prev;
            i--;
        }
    }

    return obj;
}

// 
void doubleNode::print(void (*func)(elem)) {
    node* obj = this->head;

    if (this->isEmpty()) {
        return;
    }

    while (obj) {
        func(obj->value);
        obj = obj->next;
    }
}

//  
int doubleNode::addInsert(elem* data, int pos){
	node* objPos = NULL; 
	node* objPrev = NULL;
	int i = 0;
	
	if (pos > this->size || pos < 0) 
    {
		return 0; 
	}
	
	node* obj = new node; 
	if (!obj){
		return 0;
	}
	
	if (pos == 0){
		this->push(data); 
	}
	else{
		if (pos == this->size ){
			this->unshift(data); 
		}
		else 
		{
			objPos = this->head; 
			while (objPos && i < pos) 
			{
				objPos = objPos->next; 
				i++; 
			}
			objPrev = objPos->prev;
			
			objPos->prev = obj; 
			obj->next = objPos; 
			
			objPrev->next = obj; 
			obj->prev = objPrev;
			obj->value = *data; 
			this->size++; 
		}
	}
	return 1; 
}

//  
int doubleNode::deleteInsert(elem* data, int pos){
	node* obj = NULL; 
	int i = 0; 
	
	if (pos > this->size - 1 || pos < 0){
		return 0; 
	}
	
	if (this->isEmpty()){
		return 0; 
	}
	
	if (pos == 0){
		pop(data); 
	}
	else{
		if (pos == this->size - 1){
			shift(data);
		}
		else{
			obj = this->head; 
			while (obj && i < pos){
				obj = obj->next; 
				i++; 
			}
			
			obj->prev->next = obj->next; 
			obj->next->prev = obj->prev; 
			*data = obj->value; 
			this->size--; 
			free(obj); 
		}
	}
	return 1;
}




