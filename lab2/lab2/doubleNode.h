#include <iostream>
#include <stdlib.h>

using namespace std;

typedef int elem;

struct nodeStruct {
	elem value; // �������� �����
	struct nodeStruct* next; //��������� �� ��������� ������� ������
	struct nodeStruct* prev; // ��������� �� ���������� ������� ������
};

typedef struct nodeStruct node;

class doubleNode
{
	protected:
		node* head; //������� ������
		node* tail; //���� ������
		int size; //����� ������
		
	public:
		doubleNode(); //�����������
		int isEmpty(); //�������� �� ������ ��������
		int push(elem* data); //������ � ������� ������
		int pop(elem* data); //�������� � ������� ������
		int unshift(elem* data); //������ � ����� ������
		int shift(elem* data); //�������� � ���� ������
		node* getNode(int index); //��������� �������� ������
		void print(void(*func)(elem)); //���� ������
		int addInsert(elem* data, int pos); //������ � ��������
		int deleteInsert(elem* data, int pos); //�������� � ��������
};
